**Garbage Burner - Demanda**

**Objetivo:** A memória do celular pode ser um problema, quem nunca tentou baixar um jogo novo, ou tirar uma foto e não tinha espaço para armazenar? Nosso software tenta resolver esse problema, a ideia dele é identificar e excluir arquivos inúteis, sejam arquivos duplicados, resíduos de programas já desinstalados, ou programas e arquivos que não são acessados a certo tempo.

**Publico Alvo:** O Publico alvo do nosso projeto, é basicamente qualquer um que tenha um celular, pois mesmo que a pessoa não esteja com a memoria cheia, ela pode “limpar” o celular, porém imaginamos que a maior parcela do nosso publico será composto por pessoas mais velhas, que muitas vezes não gostam de mudar de aparelho pois já estão acostumados com eles, ou pessoas de baixa renda, que não tem condições de trocar de aparelho com frequência, fazendo que com o passar do tempo a memória encha.

**Critérios de Qualidade:** Os principais critérios de qualidade que nosso software deve apresentar para que ele seja eficiente e esteja pronto para ser lançado são que ele identifique tudo que é considerado lixo, porque se ele não for capaz de identificar todos os arquivos inúteis, ele será ineficiente, logo não servira ao seu propósito, e que as funcionalidades da lista realmente funcionem, como é um software que apaga as coisas, ele não pode falhar na hora de apagar o que é para ser apagado ou apagar algo que não era ser apagado, se não nosso software vai acabar não sendo confiável.

**Riscos:** São que o software falhe e apague um arquivo importante para o usuário e o software não ter adesão do público.
